<?php
namespace Kiwi\Text;

/**
 *Sets up text module for kiwi
 *
 * @author basnik
 */
class Extension extends \Nette\DI\CompilerExtension {
	
	/**
	 * Add our services
	 */
	public function loadConfiguration() {
		
		$builder = $this->getContainerBuilder();
		$config = $this->getConfig();
		
		if(isset($config['historyLength'])){
			if(!is_int($config['historyLength']) || $config['historyLength'] < 1){
				throw new \Nette\InvalidArgumentException('Kiwi-Text: History length must be number greater than 1.');
			}
		}else{
			$config['historyLength'] = TextService::DEFAULT_HISTORY_LENGTH;
		}
		
		// register our services
		$builder->addDefinition($this->prefix('main'))->setClass('\Kiwi\Text\TextService')->setArguments(array('historyLength'=>$config['historyLength']));
		$builder->addFactoryDefinition($this->prefix('feOutputControl'))->setImplement('Kiwi\Text\Fe\ITextOutputControlFactory');
		$builder->addFactoryDefinition($this->prefix('beManagementControl'))->setImplement('Kiwi\Text\Be\ITextManagementControlFactory');
	}
}
