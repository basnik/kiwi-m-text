<?php
namespace Kiwi\Text;

use Basnik\Db\Entity;
use Basnik\Db\Service;
use Kiwi\Text\Entities\Text;

/**
 * This manages all newsletter data.
 *
 * @author basnik
 */
class TextService extends Service {

	use \Nette\SmartObject;
	
	const DEFAULT_HISTORY_LENGTH = 10;
	
	/** @var int */
	protected $historyLength;
	
	public function __construct($historyLength, \Dibi\Connection $db) {
		parent::__construct($db, "kw_text_fields", Text::class);

		$this->historyLength = $historyLength;
	}
	
	/**
	 * Gets published text item for given ident.
	 * 
	 * @param string $ident
	 * @return Entities\Text|NULL
	 */
	public function getPublishedTextItemFor($ident){
		return $this->fetchObject(
			"SELECT * FROM %n WHERE ident = %s AND status = %s",
			$this->mainTable,
			$ident,
			Entities\Text::PUBLISHED
		);
	}
	
	/**
	 * Gets full history for given ident.
	 * 
	 * @param string $ident
	 */
	public function getHistoryFor($ident){
		return $this->getAllMatching(['ident' => $ident], ['saved' => 'DESC'], $this->historyLength);
	}
	
	/**
	 * Saves  item.
	 * @param Entity $textItem
	 */
	public function save(Entity $textItem): Entity {
		
		// delete all items older than allowed history length
		$items = $this->getHistoryFor($textItem->ident);
		if($items && count($items) == $this->historyLength){
			
			$idsToKeep = array();
			foreach($items as $item){
				$idsToKeep[] = $item->id;
			}

			$this->db->query(
				"DELETE FROM %n WHERE ident = ? AND id NOT IN (?)",
				$this->mainTable,
				$textItem->ident,
				$idsToKeep
			);
		}

		if($textItem->status == Entities\Text::PUBLISHED){
			
			// if this item should be published, we need to update all existing items which are published to archive status
			$this->db->query(
				"UPDATE %n SET status = ? WHERE ident = ? AND status = ?",
				$this->mainTable,
				Entities\Text::ARCHIVE,
				$textItem->ident,
				Entities\Text::PUBLISHED
			);
			
			// if this item should be published, we need to update all existing items which are older and draft to archive status
			$this->db->query(
				"UPDATE %n SET status = ? WHERE ident = ? AND status = ? AND saved < ?",
				$this->mainTable,
				Entities\Text::ARCHIVE,
				$textItem->ident,
				Entities\Text::DRAFT,
				$textItem->saved
			);
		}

		return parent::save($textItem);
	}
	
}
