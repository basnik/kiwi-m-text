<?php

namespace Kiwi\Text\Entities;

use Basnik\Db\Entity;

/**
 * This is a rich text field
 */
class Text extends Entity {
	
	const DRAFT = 'draft';
	const PUBLISHED = 'published';
	const ARCHIVE = 'archive';

	/**
	 * Main identificator. Can also be used as a mark to retrieve data.
	 */
	public $id;

	/**
	 * Text field identifier.
	 */
	public $ident;

	/**
	 * Text field item status.
	 */
	public $status;
	
	/**
	 * Text field body.
	 */
	public $body;
	
	/**
	 * When this version was saved.
	 */
	public $saved;

	/**
	 * Converts database row obtained as array to object.
	 * @param array $input
	 * @return Text
	 */
	public static function load(array $input) {
		$text = new Text();
		$text->extractKeys($input, "id", "ident", "status", "body", "saved");
		return $text;
	}

	/**
	 * Returns associative array with properties of this object - array should be used to store the model into database.
	 * @return array
	 */
	public function save() : array {
		$result = $this->exportProps( "id", "ident", "status", "body", "saved");
		return $result;
	}
}
