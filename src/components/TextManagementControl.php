<?php
namespace Kiwi\Text\Be;

use Kiwi\Services\SystemService;
use Nette\Application\UI\Form;
use Kiwi\Text\Entities\Text;
use Nette\Utils\DateTime;

/**
 * Management of textItem and its history.
 *
 * @author basnik
 */
class TextManagementControl extends \Nette\Application\UI\Control {
	
	/** @var string */
	protected $ident;
	
	/** @var string One of constants of wysiwyg class*/
	protected $toolbar;
	
	/** @var \Kiwi\Text\TextService */
	protected $service;

	/** @var \Kiwi\Services\SystemService */
	protected $system;
	
	/** @var array */
	protected $historyItems;

	
	public function __construct($ident, $toolbar, \Kiwi\Text\TextService $service, SystemService $system,
								\Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		
		$this->service = $service;
		$this->ident = $ident;
		$this->toolbar = $toolbar;
		$this->system = $system;
		
		// load history
		$this->historyItems = $this->service->getHistoryFor($this->ident);
	}
	
	public function render(){

		// set default item as first when form is not send and there is history
		if($this['textForm']->isSubmitted()===FALSE && $this->historyItems){
			$actualVersion = reset($this->historyItems);
			$this['textForm']->setDefaults(array(
				'body' => $actualVersion->body,
				'status' => $actualVersion->status
			));
			$this->markSelectedItem($this['textForm']['history'], $actualVersion->id);
		}
		
		$this->template->setFile(__DIR__.'/../templates/components/textManagement.latte');
		$this->template->render();
	}
	
	protected function createComponentTextForm(){
		
		$form = new Form();
		
		// fetch history
		$history = array();
		if($this->historyItems){
			foreach($this->historyItems as $item){
				$history[$item->id] = $item->saved->format('j.n.Y H:i').' ('. $this->getStatusTextualRepresentation($item->status) . ')';
			}
		}
		
		$form->addSelect('history', 'Uložené verze', $history);
		$form->addWysiwyg('body', NULL, $this->toolbar)
				->showCodeView(TRUE);
		$form->addSelect('status', 'Stav verze', array(
			Text::DRAFT => $this->getStatusTextualRepresentation(Text::DRAFT),
			Text::PUBLISHED => $this->getStatusTextualRepresentation(Text::PUBLISHED),
			Text::ARCHIVE => $this->getStatusTextualRepresentation(Text::ARCHIVE),
		))->setOption('description', sprintf('Text publikujete zvolením položky %s z tohoto výběru a uložením.', $this->getStatusTextualRepresentation(Text::PUBLISHED)));
		
		$form->addSubmit('save', 'Uložit jako novou verzi');
		$form->addSubmit('show', 'Zobrazit verzi');
		
		$form->addProtection();
		
		$form->onSuccess[] = [$this, "proccessTextForm"];
		
		if(empty($history)){
			$form['history']->setDisabled(TRUE)->setPrompt('Historie je prázdná');
			$form['show']->setDisabled(TRUE);
		}
		
		return $form;
	}
	
	public function proccessTextForm(Form $form, $values){
		
		if($form['save']->isSubmittedBy()){
			
			$textItem = new Text();
			$textItem->ident = $this->ident;
			$textItem->status = $values->status;
			$textItem->body = $values->body;
			$textItem->saved = new DateTime();
			
			$this->service->save($textItem);
			$this->flashMessage('Uloženo. Vytvořena nová verze v historii.', 'success');

			$this->system->logActivity(sprintf("Nová verze textového pole %s (%d) byla uložena. Stav verze po uložení: %s.",
				$textItem->ident, $textItem->id, $textItem->status));
			
			if($this->presenter->isAjax()){
				$this->redrawControl();
			}else{
				$this->redirect('this');
			}
			
		}else if($form['show']->isSubmittedBy()){
			
			foreach($this->historyItems as $item){
				if($item->id == $values->history){
					$form->setValues(array(
						'history' => $item->id,
						'body' => $item->body,
						'status' => $item->status
					));
					$this->markSelectedItem($form['history'], $values->history);
					break;
				}
			}
		}
	}
	
	/**
	 * Gets textual representation of Text entity constants
	 * 
	 * @param string $status
	 * @return string
	 */
	protected function getStatusTextualRepresentation($status){
		switch($status){
			case Text::ARCHIVE:
				return 'archivováno';
			case Text::DRAFT:
				return 'rozpracováno';
			case Text::PUBLISHED:
				return 'publikováno';
		}
		
		return '';
	}
	
	/**
	 * Mark selected item in select box.
	 * 
	 * @param \Nette\Forms\Controls\SelectBox $select
	 * @param int $itemKey
	 */
	protected function markSelectedItem(\Nette\Forms\Controls\SelectBox $select, $itemKey){
		
		$items = $select->getItems();
		$items[$itemKey] .= ' - zobrazeno';
		$select->setItems($items);
	}
	
}
