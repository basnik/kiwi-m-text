<?php
namespace Kiwi\Text\Fe;

/**
 * Outputs last published textItem for given ident.
 *
 * @author basnik
 */
class TextOutputControl extends \Nette\Application\UI\Control{
	
	/** @var string */
	protected $ident;
	
	/** @var \Kiwi\Text\TextService */
	protected $service;
	
	public function __construct($ident, \Kiwi\Text\TextService $service, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		$this->ident = $ident;
		$this->service = $service;
	}

	public function render(){
		
		$this->template->textItem = $this->service->getPublishedTextItemFor($this->ident);
		
		$this->template->setFile(__DIR__.'/../templates/components/textOutput.latte');
		$this->template->render();
	}

}
