<?php

namespace Kiwi\Text\Fe;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface ITextOutputControlFactory {
	
	/** 
	 * @param $ident
	 * @return TextOutputControl 
	 */
	public function create($ident);
}
