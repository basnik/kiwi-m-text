<?php

namespace Kiwi\Text\Be;

use Kiwi\Wysiwyg;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface ITextManagementControlFactory {
	
	/** 
	 * @param $ident
	 * @param $toolbar
	 * @return TextManagementControl 
	 */
	public function create($ident, $toolbar=Wysiwyg::TOOLBAR_BASIC);
}
